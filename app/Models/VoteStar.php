<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VoteStar extends Model
{
    use HasFactory;
    protected $table='vote_star';
}
