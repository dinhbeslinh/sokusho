import axios from "axios"

export  const getLoginAPI = async(userLogin) => {
    const res = await axios.post('https://dokusho.examtoeic.online/api/admin/login',userLogin)
    return res.data
}
