<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected  $fillable = ['category_name','decription','slug_category'];
    protected $table ='category';
    public function story()
    {
        return $this->belongsToMany(Story::class,'story_categoties','category_id','story_id');
    }
}
