<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'google' => [
        'client_id'     => '204423813847-4u84kpcttcpqgab1okrq84ccsicop5ao.apps.googleusercontent.com',
        'client_secret' => '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',
        'redirect'      => 'https://kimthong.club/api/callback'
    ],

    'facebook' => [
        'client_id' => '476016077882497',
        'client_secret' => '6ccc6d0fe4cd67c4fae009b49d949670',
        'redirect' => 'https://kimthong.club/api/facebook/callback/'
    ],
];
