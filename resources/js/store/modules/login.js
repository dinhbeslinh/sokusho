import axios from 'axios';
import {getLoginAPI} from '../../api/loginAPI'

const state = () => {
    return {
        userLogin: {}
    }
}

const mutations = {
    setUserLogin(state, data) {
        state.userLogin = data
        localStorage.setItem('userLogin',JSON.stringify(data))
    },
    setUserLoginLocalStorage(state,payload){
        state.userLogin = payload
    }
}

const actions = {
    async signInAction({commit}, {data, router}) {
        try {
            const userLogin = await getLoginAPI(data);
            console.log(userLogin.data)
            switch(userLogin.code){
                case 200:
                    router.push("/table");
                    commit("setUserLogin", userLogin)
                    break
                default :
                    alert(`${userLogin.message}`)
                    break
            }
        } catch(error){

        }
    },
    loadUserLoginFromLocalStorage({commit}){
        let userLogin = null;
        if(localStorage.getItem("userLogin")){
            userLogin = JSON.parse(localStorage.getItem("userLogin"))
        }
        commit("setUserLoginLocalStorage",userLogin)
    }
}

export default {
    namespaced: true,
    state,
    mutations,
    actions
}
