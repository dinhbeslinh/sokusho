<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ListLockChapter extends Model
{
    use HasFactory;
    protected $table = 'list_lock_chapter';
    protected $fillable = ['user_id','story_id','number','status'];
}
