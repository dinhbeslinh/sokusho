<?php

namespace App\Http\Controllers\Api\V1;

use App\common\AppConstant;
use App\Http\Requests\UserUpdateRequest;
use App\Models\ListLockChapter;
use App\Models\Story;
use App\Response\EmptyResponse;
use App\Response\GetCoinResponse;
use App\Response\UserResponse;
use App\User;
use App\Models\SavedStory;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\UserComment;
use App\Models\Coin;
use App\Models\VoteStar;

class UserController extends Controller
{
    //register
    public function register(Request $request)
    {
        $validEmail = User::WHERE([
            'email' => $request['email']
        ])->get();
        if (sizeof($validEmail)) {
            return response()->json([
                'code_status' => 400,
                'message' => 'Email đã tồn tại',
                'data' => null
            ], 400);
        }
        $user = new User();
        $user['email'] = $request['email'];
        $user['name'] = $request['name'];
        $user['password'] = bcrypt($request['password']);
        $res = $user->save();
        if (!isset($res)) {
            return response()->json([
                'code_status' => 400,
                'message' => 'Failed',
                'data' => null
            ], 400);
        }
        return response()->json([
            'code_status' => 200,
            'message' => 'Success',
            'data' => $user
        ], 200);
    }

    //login
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        if (!($token = JWTAuth::attempt($credentials))) {
            return response()->json([
                'code_status' => 400,
                'message' => 'Sai tên tài khoản hoặc mật khẩu',
                'token' => null
            ], 400);
        }

        return response()->json([
            'code_status' => 200,
            'message' => 'Success',
            'token' => $token,
        ], 200);
    }

    //logout
    public function logout(Request $request)
    {
        $this->validate($request, ['token' => 'required']);

        try {
            JWTAuth::invalidate($request->input('token'));
            return response()->json('Success', Response::HTTP_OK);
        } catch (JWTException $e) {
            return response()->json('Failed', Response::HTTP_BAD_REQUEST);
        }
    }

    //get_user_info
    public function getUserInfo()
    {
        $data = [];
        $user = Auth::user();
        $data['id'] = $user['id'];
        $data['name'] = $user['name'];
        $data['email'] = $user['email'];
        $data['nick_name'] = $user['nick_name'];
        $data['coin'] = $user['coin'];
        $data['date_of_birth'] = $user['date_of_birth'];
        $data['avatar'] = $user['avatar'];

        if ($user == null) {
            return response()->json([
                'code_status' => 400,
                'message' => 'Failed',
                'data' => null
            ], 400);
        }

        return response()->json([
            'code_status' => 200,
            'message' => 'Success',
            'data' => $data
        ], 200);
    }

    //refresh_token
    public function refresh()
    {
        return response(JWTAuth::getToken(), Response::HTTP_OK);
    }

    public function updateUserInfo(Request $request)
    {
        $user = Auth::user();
        $user['name'] = $request['name'] == null ? $user['name'] : $request['name'];
        $user['email'] = $request['email'] == null ? $user['email'] : $request['email'];
        $user['date_of_birth'] = $request['date_of_birth'] == null ? $user['date_of_birth'] : $request['date_of_birth'];
        $user['nick_name'] = $request['nick_name'] == null ? $user['nick_name'] : $request['nick_name'];
        if (!is_null($request['avatar'])) {
            $destination_path = 'public/images/user';
            $file = $request->file('avatar');
            $filename = $file->getClientOriginalName();
            $request->file('avatar')->storeAs($destination_path, $filename);
            $public = "https://dokusho.examtoeic.online";
            $user['avatar'] = "$public/storage/images/user/$filename";
        }
        $user->save();
        $data = [];
        $data['id'] = $user['id'];
        $data['name'] = $user['name'];
        $data['email'] = $user['email'];
        $data['nick_name'] = $user['nick_name'];
        $data['coin'] = $user['coin'];
        $data['date_of_birth'] = $user['date_of_birth'];
        $data['avatar'] = $user['avatar'];

        if ($user == null) {
            return response()->json([
                'code_status' => 400,
                'message' => 'Failed',
                'data' => null
            ], 400);
        }
        return response()->json([
            'code_status' => 200,
            'message' => 'Success',
            'data' => $data
        ], 200);
    }

    //get-save-story
    public function getSaveStory()
    {
        $user = Auth::user();
        $user_id = $user['id'];
        $bookmarks = SavedStory::query()->where('user_id', $user_id)->select('story_id')->get();
        foreach ($bookmarks as $item) {
            $data[] = Story::query()->where('id', $item['story_id'])->select('id', 'story_image', 'story_name', 'story_name_author', 'story_vote_star')->first();
        }
        return response()->json([
            'status' => 200,
            'message' => 'Success',
            'data' => $data], 200);
    }

    //post-save-story
    public function postSaveStory(Request $request)
    {
        $dataRequest = $request->all();
        $validator = Validator::make($dataRequest, [
            'story_id' => 'required|integer',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 404);
        }
        $user = Auth::user();
        $user_id = $user['id'];
        $story_id = $request['story_id'];
        $saved_story = new SavedStory();
        $saved_story['story_id'] = $story_id;
        $saved_story['user_id'] = $user_id;
        $data = SavedStory::query()->where(['user_id' => $user_id])->get();
        foreach ($data as $item) {
            if ($item['story_id'] == $story_id) {
                SavedStory::query()->where('story_id', $story_id)->where('user_id', $user_id)->delete();
                return response()->json(['status' => 200, 'message' => 'Truyện đã bỏ lưu'], 200);
            }
        }
        $saved_story = new SavedStory();
        $saved_story['story_id'] = $request['story_id'];
        $saved_story['user_id'] = $user_id;
        $saved_story->save();
        return response()->json(['status' => 200, 'data' => $saved_story, 'message' => 'Truyện đã lưu'], 200);
    }

    public function getUserComment(Request $request)
    {
        $story_id = $request->input('story_id');
        $comment = UserComment::query()->where('story_id', $story_id)->get();
        return response::json(['status' => 200, 'data' => $comment, 'message' => "Success"], 200);
    }

    public function postUserComment(Request $request)
    {
        $dataRequest = $request->all();
        $validator = Validator::make($dataRequest, [
            'story_id' => 'required|integer',
            'comment' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 404);
        }
        $user = Auth::user();
        $user_id = $user['id'];
        $user_name = $user['name'];

        $res = new UserComment();
        $res['story_id'] = $request['story_id'];
        $res['user_id'] = $user_id;
        $res['user_name'] = $user_name;
        $res['comment'] = $request['comment'];
        $res->save();

        $number_comment = UserComment::query()->where('story_id', $request['story_id'])->count();
        $update_number_comment = Story::query()->find($request['story_id']);
        $update_number_comment['number_comment'] = $number_comment;
        $update_number_comment->save();

        if (isset($res)) {
            return response()->json([
                'code_status' => 200,
                'message' => 'Success',
                'data' => $res
            ], 200);
        }

        return response()->json([
            'code_status' => 400,
            'message' => 'Failed',
            'data' => null
        ], 400);
    }

    //get_coin
    public function getCoin()
    {
        $user = Auth::user();
        if ($user == null) {
            return response()->json([
                'code_status' => 400,
                'message' => 'Failed'
            ], 400);
        }
        $coin = Coin::query()->select('id', 'xu', 'USD')->get();
        $data = [];
        $data['user_id'] = $user['id'];
        $data['coin'] = $user['coin'];
        $data['add_coin'] = $coin;

        return response()->json([
            'code_status' => 200,
            'message' => 'Success',
            'data' => $data
        ], 200);
    }

    //post_coin
    public function postAddCoin(Request $request)
    {
        $user = Auth::user();
        $coins = Coin::where('id', $request['id'])->first();
        $data = $user['coin'] = $user['coin'] + $coins['xu'];
        $xu = $coins['xu'];
        $user->save();
        if ($user) {
            return response()->json([
                'code_status' => 200,
                'message' => "Nạp $xu xu thành công",
                'data' => $data
            ], 200);
        }
        return response()->json([
            'code_status' => 400,
            'message' => 'Failed',
            'data' => null
        ], 400);
    }

    //post_vote_star
    public function postVoteStar(Request $request)
    {
        $dataRequest = $request->all();
        $validator = Validator::make($dataRequest, [
            'story_id' => 'required|integer',
            'vote_star' => 'required|integer'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 404);
        }
        $story_id = $request['story_id'];
        $user = Auth::user();
        $id_user = $user['id'];
        $data = VoteStar::query()->where(['user_id' => $id_user])->get();
        foreach ($data as $item) {
            if ($item['story_id'] == $story_id) {
                return response()->json('Đã vote');
            }
        }
        $star_voting = new VoteStar();
        $star_voting['vote_star'] = $request['vote_star'];
        $star_voting['story_id'] = $request['story_id'];
        $star_voting['user_id'] = $id_user;
        $star_voting->save();

        $vote = VoteStar::query()->where('story_id', $request['story_id'])->avg('vote_star');
        $avg_vote = number_format($vote);
        $story_star = Story::query()->find($request['story_id']);
        $story_star['story_vote_star'] = $avg_vote;
        $story_star->save();
        $user_voted = [];
        $user_voted['vote_star'] = $star_voting['vote_star'];
        $user_voted['vote_story'] = $story_star['story_vote_star'];
        return response()->json(['status' => 200, 'data' => $user_voted, 'message' => 'Success'], 200);
    }

    public function readChapter(Request $request)
    {
        $user = Auth::id();
        $story_id = $request['story_id'];
        $data = ListLockChapter::query()->where(['user_id' => $user, 'story_id' => $story_id])->get();
        foreach ($data as $item) {
            if ($item['story_id'] == $story_id) {
                return response()->json([
                    'status_code' => 400,
                    'data' => null,
                    'message' => "Failed",
                ], 400);
            }
        }
        $getChapter = DB::table('chapter')->where('story_id', $story_id)->select('number', 'story_id', 'status')->get();
        foreach ($getChapter as $records) {
            DB::table('list_lock_chapter')->insert(get_object_vars($records));
        }
        ListLockChapter::where('story_id', '=', $story_id)->where('user_id', '=', null)
            ->update([
                'user_id' => $user,
            ]);
        $lock = ListLockChapter::query()->where('user_id', $user)->where('story_id', $story_id)->get();
        return response()->json([
            'status_code' => 200,
            'data' => $lock,
            'message' => "Success",
        ], 200);
    }

    public function unlockAllChapter(Request $request)
    {
        $user = Auth::user();
        $user_id = $user['id'];
        $story_id = $request['story_id'];
        $story = Story::where('id', $story_id)->first();
        //
        $count = ListLockChapter::where('story_id', '=', $story_id)->where('user_id', '=', $user_id)->count();
        //
        if ($user['coin'] - $count * $story['price'] >= 0 && $count == true) {
            $user['coin'] = $user['coin'] - $count * $story['price'];
            $user->save();
            ListLockChapter::where('story_id', '=', $story_id)->where('user_id', $user_id)
                ->update([
                    'status' => 2
                ]);
            return response()->json([
                'status_code' => 200,
                'data' => $user['coin'],
                'message' => "Success",
            ], 200);
        }
        return response()->json([
            'status_code' => 400,
            'data' => null,
            'message' => "Failed",
        ], 400);
    }

    public function unlockOneChapter(Request $request)
    {
        $user = Auth::user();
        $user_id = $user['id'];
        $story_id = $request['story_id'];
        $number = $request['number'];
        $story = Story::where('id', $story_id)->first();
        $count = ListLockChapter::where('story_id', '=', $story_id)->where('user_id', '=', $user_id)->where('number', '=', $number)->count();
        //
        if ($user['coin'] - $count * $story['price'] >= 0 && $count == true) {
            $user['coin'] = $user['coin'] - $count * $story['price'];
            $user->save();
            ListLockChapter::where('story_id', '=', $story_id)->where('user_id', '=', $user_id)->where('number', '=', $number)
                ->update([
                    'status' => 2
                ]);
            return response()->json([
                'status_code' => 200,
                'data' => $user['coin'],
                'message' => "Success",
            ], 200);
        }
        return response()->json([
            'status_code' => 400,
            'data' => null,
            'message' => "Failed",
        ], 400);
    }
}


