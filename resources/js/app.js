require('./bootstrap');

import {createApp} from 'vue';
import App from '../js/App.vue';
import store from './store';
import router from "./router";
import ModalAdd from './components/ModalAdd.vue';
import axios from 'axios';
window.axios = axios;
import 'ant-design-vue/dist/antd.css';
import '../../node_modules/font-awesome/css/font-awesome.min.css'
import '../../public/style.css'

import {
    Table,
    Button,
    message
} from 'ant-design-vue'

import '../../public/app.css';
import '../../public/css/grid.css';

const app = createApp(App);
app.use(store);
app.use(router);
app.mount('#app');
app.component("modal",ModalAdd)
app.use(Button)
app.use(Table)
app.config.globalProperties.$message = message;
