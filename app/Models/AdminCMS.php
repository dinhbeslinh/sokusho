<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdminCMS extends Model
{
    use HasFactory;
    protected $table='admin_cms';

    protected $hidden = [
        'password', 'remember_token','updated_at', 'created_at'
    ];

}
