<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ListStory extends Model
{
    protected $table = 'list_story';
    protected $hidden = [
       'updated_at', 'created_at'
    ];

    public function list_story()
    {
        return $this->hasMany(Story::class,"liststory_id","id");
    }


}

