<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Category;
use GrahamCampbell\ResultType\Success;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function getCategory()
    {
        $data = Category::query()->select('id', 'icon', 'category_name')->get();
        return Response::json(['status' => 200, 'title' => "Chào bạn mới!", 'suggest' => "Hãy chọn ra khoảng 2 thể loại bạn ưa thích. Điều này sẽ giúp Dokusho gợi ý những tác phẩm phù hợp cho bạn", 'message' => 'Success', 'data' => $data,], 200);
    }
}
