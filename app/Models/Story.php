<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\StoryCategory;

class Story extends Model
{
    use HasFactory;
    protected $table = 'story';

    protected $hidden = [
        'updated_at', 'created_at'
    ];
    protected $fillable = ['story_name','story_author_name','content_story','price'];

    public function Chapter()
    {
        return $this->hasMany(Chapter::class);
    }
    public function Chapter_list()
    {
        return $this->hasMany(Chapter::class)->select(['id','number','name_chapter','slug_chapter','content_chapter','story_id'])->orderBy('chapter.number', 'ASC');
    }
    public function Story_Category()
    {
        return $this->hasMany(StoryCategory::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }



}
