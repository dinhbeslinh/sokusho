<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StoryCategory extends Model
{
    use HasFactory;
    protected $fillable = ['category_id','story_id'];
    protected $table ='story_categories';
    public function story_category_many()
    {
        return $this->belongsTo(Story::class);
    }

}
