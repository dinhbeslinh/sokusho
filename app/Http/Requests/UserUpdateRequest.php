<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string',
            'email' =>'email',
            'avatar' => 'image|mimes:jpeg,png,jpg,svg|max:5120'
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.string' => 'name is string!',
            'email'=>'email is email',
            'ios_device_token.string' => 'name is string!',
            'android_device_token.string' => 'name is string!',
            'avatar.image'   =>  'File upload is not image format!',
            'avatar.max' => 'File upload size < 2mb'
        ];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(
            response()->json([
                'code_status'   =>  Response::HTTP_BAD_REQUEST,
                'msg' => $errors
            ], Response::HTTP_OK)
        );
    }
}
