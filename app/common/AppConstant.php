<?php


namespace App\common;


class AppConstant
{
    public static $SUCCESS = "Success!";
    public static $FAILED = "Failed!";
    public static $NO_DATA   = "null!";

    public static $LOGIN_SUCCESS = "Đăng nhập thành công !";
    public static $LOGIN_FAILED = "Sai tên tài khoản hoặc mật khẩu !";

    public static $VALID_EMAIL   = "Email này đã có người đăng kí!";
    public static $REGISTER_SUCCESS   = "Success!";

    public static $UPLOAD_FAILED   = "Upload Failed!";
    public static $UPLOAD_SUCCESS   = "Upload Success!";

    public static $CREATE_SUCCESS = "Create Successful!";
    public static $CREATE_FAILED  = "Create Failed!";

    public static $UPDATE_SUCCESS = "Update Successful!";
    public static $UPDATE_FAILED  = "Update Failed!";

    public static $DELETE_FAILED  = "Delete Failed!";
    public static $DELETE_SUCCESS = "Delete Successful!";

    public static $EXIST = "Đã tồn tại";
    public static $CHANGE_PASSWORD_SUCCESS = "Đổi mật khẩu thành công!";

    public static $CODE_FAILED = "language_code đã tồn tại không được trùng lặp";
}
