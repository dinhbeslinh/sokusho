<?php

namespace App\Http\Controllers\Admin\V1;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Chapter;
use App\Models\Story;
use App\Models\StoryCategory;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CMSController extends Controller
{
    public function getHomeCMS()
    {
        $dd = [];
        $data = [];
        $dataStory = Story::query()->get();
        foreach ($dataStory as $item) {
            $number_chapter = Chapter::query()->where(['story_id' => $item->id])->select('number')->max('number');
            $number_chapter_free = Chapter::query()->where(['story_id' => $item->id])->where('status', '1')->count();
            $id_category = StoryCategory::query()->where(['story_id' => $item->id])->select('category_id')->first();
            $category = Category::query()->where('id', $id_category['category_id'])->select('category_name')->first();
            $dd['id'] = $item['id'];
            $dd['story_name'] = $item['story_name'];
            $dd ['category'] = $category['category_name'];
            $dd ['number_chapter'] = $number_chapter;
            $dd ['number_chapter_free'] = $number_chapter_free;
            $dd['price'] = $item['price'];
            $data[] = $dd;
        }
        return Response::json(['status' => 1, 'data' => $data, 'message' => "Success"],200);
    }
}
