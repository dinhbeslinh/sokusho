import {getMangaApi} from '../../api/manga';
import {createMangaApi} from '../../api/manga'
import {editMangaApi} from '../../api/manga'
const state = () => {
    return {
        mangaList: [],
        mangaDetail: {}
    }
}

const mutations = {
    setMangaMutation(state, payload) {
        state.mangaList = payload
    },
    setMangaDetail(state,payload){
        state.mangaDetail = payload
    }
}

const actions = {
    async getMangaAction({commit}) {
        const payload = await getMangaApi({});
        commit('setMangaMutation', payload)
        console.log(payload)
    },
    async createMangaAction(context,payload){
        const newManga = await createMangaApi(payload);
        console.log(newManga)
    },
    async editMangaAction({commit},payload){
        const mangaDetail = await editMangaApi(payload)
        commit('setMangaDetail',mangaDetail)
    }
}


export default {
    namespaced: true,
    state,
    mutations,
    actions
}
