import { createRouter, createWebHistory } from "vue-router";
import ModalAdd from "../components/ModalAdd";
import TableCategory from '../components/TableCategory'
import SignIn from "../view/SignIn"

const routes = [

    {
        path: "/",
        name: "SignIn",
        component: () => import('../view/SignIn'),
    },
    {
        path: "/client/:id",
        name: "ClientEdit",
        component: () => import('../view/ClientEdit'),
        props: true,
    },
    {
        path: '/table',
        name: 'TableCategory',
        component: () => import('../components/TableCategory'),

    }
]


const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes,
    linkActiveClass:"active",
});

export default router;
