<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StoryChapter extends Model
{
    use HasFactory;
    protected $table ='story_chapter';
}
