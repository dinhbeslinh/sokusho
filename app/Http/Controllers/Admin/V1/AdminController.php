<?php

namespace App\Http\Controllers\Admin\V1;

use App\common\AppConstant;
use App\Http\Controllers\Controller;
use App\Response\EmptyResponse;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use App\Models\Admin;
use Illuminate\Support\Facades\Hash;
use App\Models\AdminCMS;

class AdminController extends Controller
{
    public function loginAdmin(Request $request)
    {
        if ($request->isMethod("post")) {
            $email = $request->input('email');
            $password = $request->input('password');
            $admin = AdminCMS::query()->where(['email' => $email])->first();
            if (!empty($admin)) {
                $hashPass = $admin->password;
                if (Hash::check($password, $hashPass)) {
                    if (empty($admin)) {
                        return response()->json([
                            'status_code' => 400,
                            'message' => 'Failed'
                        ]);
                    } else {
                        return response()->json([
                            'status_code' => 200,
                            'message' => 'Success',
                            'data' => $admin,
                        ], 200);
                    }
                } else {
                    return response()->json([
                        'status_code' => 201,
                        'message' => 'Password incorrect'
                    ], 200);
                }
            } else {
                return response()->json([
                    'status_code' => 400,
                    'message' => 'Password incorrect',
                    'error' => "User doesn't exist"], 400);
            }
        } else {
            return response()->json([
                'status_code' => 400,
                'message' => 'Failed'],400);
        }
    }
}

