<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravelista\Comments\Commentable;

class Chapter extends Model
{
    use HasFactory;
    use Commentable;
    protected $table = 'chapter';
    protected $fillable = ['number','name_chapter','content_chapter','story_id','status','number_chapter'];
    public function story()
    {
        return $this->belongsTo(Story::class);
    }

}

