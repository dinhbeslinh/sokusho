<?php

namespace App\Http\Controllers\Admin\V1;

use App\common\AppConstant;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Chapter;
use App\Models\StoryCategory;
use App\Models\StoryChapter;
use Illuminate\Http\Request;
use App\Models\Story;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Response;

class EditStoryController extends Controller
{
    //create
    public function createCMS(Request $request)
    {
        $dataRequest = $request->all();
        $validator = Validator::make($dataRequest, [
            'story_name' => 'required|unique:story',
            'category_id' => 'required',
        ]);
        if ($validator->fails()) {
            return Response::json($validator->errors(), 404);
        }
        $storyCreate = new Story();
        $storyCreate['story_name'] = $request['story_name'];
        $storyCreate['story_name_author'] = $request['story_name_author'];
        $storyCreate['content_story'] = $request['content_story'];
        $storyCreate['price'] = $request['price'];
        $res = $storyCreate->save();

        $id_story = $storyCreate['id'];
        $story = Story::query()->find($storyCreate['id']);
        foreach ($request['category_id'] as $category_id) {
            $story->Story_Category()->create([
                'category_id' => $category_id
            ]);
        }
        foreach ($dataRequest['chapter'] as $chapter) {
            $chapters = new Chapter();
            $chapters['content_chapter'] = $chapter['content_chapter'];
            $chapters['story_id'] = $storyCreate['id'];
            $chapters['status'] = $chapter['status'];
            $end_chapter = Chapter::where('story_id', $chapters['story_id'])->orderBy('id', 'DESC')->first();
            $num = 1;
            if ($end_chapter == null)
                $chapters['number'] = $num;
            else {
                $num = $end_chapter->number;
                $num = $num + 1;
                $chapters['number'] = $num;
            }
            $chapters['number_chapter'] = "Chương $num";
            $chapters['slug_chapter'] = "chuong-$num";
            $chapters->save();
            $id_chapter = $chapters['id'];

            $story_chapter = new StoryChapter();
            $story_chapter['chapter_id'] = $id_chapter;
            $story_chapter['story_id'] = $id_story;
            $story_chapter->save();
        }
        if (!isset($res)) {
            return Response()->json([
                'code_status' => 400,
                'message' => 'Failed',
            ], 400);
        }
        return Response()->json([
            'code_status' => 200,
            'message' => 'Success',
        ], 200);
    }

    //edit
    public function editCMS(Request $request)
    {
        $story_id = $request->input('story_id');
        $data = [];
        $dataStory = Story::where('id', $story_id)->get();
        $category_id = StoryCategory::query()->where('story_id',$story_id)->select('category_id')->get();
        $chapters = Chapter::query()->where('story_id', $story_id)->select('id', 'number', 'name_chapter', 'content_chapter')->get();
        foreach ($dataStory as $item) {
            $data ['id_story'] = $item['id'];
            $data ['story_name'] = $item['story_name'];
            $data ['story_image'] = $item['story_image'];
            $data ['story_name_author'] = $item['story_name_author'];
            $data ['content_story'] = $item['content_story'];
            $data ['price'] = $item['price'];
            $data ['category'] = $category_id;
            $data ['chapter'] = $chapters;
        }
        return Response::json(['status_code'=>200,'message'=>'success','data'=>$data],200);
    }

    //update
    public function updateCMS(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'story_name' => 'required|unique:story',
            'category_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 404);
        }
        $story_id = $request->input('story_id');
        $story = Story::query()->find($story_id);
        $story['story_name'] = $request['story_name'];
        $story['story_name_author'] = $request['story_name_author'];
        $story['content_story'] = $request['content_story'];
        $story['price'] = $request['price'];
        $res = $story->save();

        StoryCategory::where('story_id',$story_id)->delete();
        Chapter::where('story_id',$story_id)->delete();

        foreach ($request['category_id'] as $category_id) {
            $story->Story_Category()->create([
                'category_id' => $category_id
            ]);
        }

        foreach ($request['chapter'] as $chapter) {
            $chapters = new Chapter();
            $end_chapter = Chapter::query()->where('story_id', $story['id'])->orderBy('number', 'DESC')->first();
            $num = 1;
            if ($end_chapter == null)
                $chapters['number'] = $num;
            else {
                $num = $end_chapter['number'];
                $num = $num + 1;
                $chapters['number'] = $num;
            }
            $story->Chapter()->create([
                'content_chapter'=>$chapter['content_chapter'],
                'status'=>$chapter['status'],
                'number'=>$chapters['number'],
                'slug-chapter' => "chuong-$num",
                'number-chapter' =>"Chương $num"
            ]);
        }

        if (!isset($res)) {
            return Response()->json([
                'code_status' => 400,
                'message' => 'Failed',
            ], 400);
        }
        return Response()->json([
            'code_status' => 200,
            'message' => 'Success',
        ], 200);

    }

    //delete
    public function deleteCMS(Request $request)
    {
        $story_id=$request->input('story_id');
        $story=Story::query()->where('id', $story_id)->first();
        if(isset($story)){
            Story::query()->where('id', $story_id)->delete();
            return response()->json([
                'status_code'=>200,
                'message'=>'Xoá thành công'
            ],200);
        }
        return response()->json([
            'code_status' => 400,
            'message' => 'Không tồn tại',
        ],400);
    }

    public function searchCMS(Request $request){
        $data = $request->get('data');
        $search = Story::query()->where('story_name','like',"%{$data}%")->select('story_name')->get();
        return response()->json([
            'status_code'=>200,
            'message'=>'Success',
            'data'=>$search
        ]);
    }
}
