<?php

namespace App\Http\Controllers\Api\V1;

use App\common\AppConstant;
use App\Http\Controllers\Controller;
use App\Models\Chapter;

use App\Models\UserComment;
use App\Response\EmptyResponse;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use App\Models\Story;
use App\Models\ListStory;
use App\Models\Category;
use App\Models\StoryCategory;


class MyHomeController extends Controller

{
    public function getStory()
    {
        $story_new = Story::where('story_new', '1')->select(['id', 'story_name', 'story_image', 'story_name_author', 'story_vote_star', 'category_id'])->get();
        $story_read_most = Story::where('story_read', '1')->select(['id', 'story_name', 'story_image', 'story_name_author', 'story_vote_star', 'category_id'])->get();
        $story_recommend = Story::where('story_recommend', '1')->select(['id', 'story_name', 'story_image', 'story_name_author', 'story_vote_star', 'category_id'])->get();
        $story_distcount = Story::where('story_distcount', '1')->select(['id', 'story_name', 'story_image', 'story_name_author', 'story_vote_star', 'category_id'])->get();
        $story_free = Story::where('story_free', '1')->select(['id', 'story_name', 'story_image', 'story_name_author', 'story_vote_star', 'category_id'])->get();
        return Response::json(['status' => 200, 'truyện mới' => $story_new, 'truyện đọc nhiều' => $story_read_most, 'truyện recommend' => $story_recommend, 'truyện distcount' => $story_distcount, 'truện free' => $story_free, 'message' => 'Success'], 200);
    }

    public function infoStory(Request $request)
    {
        $story_id = $request->input('story_id');
        $dd = [];
        $dataStory = Story::where('id', $story_id)->get();
        foreach ($dataStory as $item) {
            $category_id = StoryCategory::query()->where(['story_id' => $item->id])->select('category_id')->get();
            foreach ($category_id as $item2) {
                $category_name[] = Category::query()->where(['id' => $item2->category_id])->select('id', 'category_name')->first();
            }
            $dd ['id'] = $item['id'];
            $dd ['story_name'] = $item['story_name'];
            $dd ['story_image'] = $item['story_image'];
            $dd ['story_name_author'] = $item['story_name_author'];
            $dd ['category'] = $category_name;
            $dd ['content_story'] = $item['content_story'];
            $dd ['number_comment'] = $item['number_comment'];
            $dd ['story_vote_star'] = $item['story_vote_star'];
        }
        if (empty($dd)) {
            return response()->json([
                'code_status' => 400,
                'message' => 'Failed',
                'data' => null
            ], 400);
        }
        return response()->json([
            'code_status' => 200,
            'message' => 'Success',
            'data' => $dd,
        ], 200);
    }

    public function getTopic()
    {
        $data = ListStory::query()->get();
        foreach ($data as $item) {
            $item['icon'] = url($item->icon);
        }
        if (empty($data)) {
            return response()->json([
                'code_status' => 0,
                'data' => null,
                'message' => AppConstant::$FAILED,
            ], 404);
        }
        return response()->json([
            'code_status' => 1,
            'data' => $data,
            'message' => AppConstant::$SUCCESS,
        ], 200);
    }


    public function getHomeStory(Request $request)
    {
        $topic_id = $request->input("topic_id");
        $data = ListStory::query()->where('id', $topic_id)->with(['list_story' => function ($q) {
            $q->get();
        }])->get();

        foreach ($data as $item) {
            foreach ($item->list_story as $ss) {
                $ss['story_image'] = url($ss->story_image);
            }
        }
        if (empty($data[0]->list_story)) {
            return response()->json([
                'code_status' => 0,
                'data' => null,
                'message' => AppConstant::$FAILED,
            ], 404);
        }
        return response()->json([
            'code_status' => 1,
            'data' => $data[0]->list_story,
            'message' => AppConstant::$SUCCESS,
        ], 200);

    }

    public function getListChapter(Request $request)
    {
        $story_id = $request->input('story_id');
        $data = Chapter::query()->where('story_id', $story_id)->select('id', 'story_id', 'number', 'name_chapter', 'status')->orderBy('number', 'ASC')->get();
        if (empty($data)) {
            return response()->json([
                'code_status' => 400,
                'message' => 'Failed',
                'data' => null,
            ], 400);
        }
        return response()->json([
            'code_status' => 200,
            'message' => 'Success',
            'data' => $data
        ], 200);
    }

    public function getContentChapter(Request $request)
    {
        $story_id = $request->input('story_id');
        $chapter_id = $request->input('number');
        $data = Chapter::query()->where('story_id', $story_id)->where('number', $chapter_id)->select('id', 'story_id', 'number', 'name_chapter', 'content_chapter')->first();
        if (empty($data)) {
            return response()->json([
                'code_status' => 400,
                'data' => null,
                'message' => 'Failed',
            ], 400);
        }
        return response()->json([
            'code_status' => 200,
            'message' => 'Success',
            'data' => $data
        ], 200);
    }
}
