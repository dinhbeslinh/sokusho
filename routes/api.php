<?php

use App\Http\Controllers\Admin\V1\AdminController;
use App\Http\Controllers\Admin\V1\CMSController;
use App\Http\Controllers\Api\V1\CategoryController;
use App\Http\Controllers\Api\V1\MyHomeController;
use App\Http\Controllers\Api\V1\PasswordResetController;
use App\Http\Controllers\Api\V1\SocialsController;
use App\Http\Controllers\Api\V1\UserController;
use App\Http\Controllers\Admin\V1\EditStoryController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'namespace' => 'Api\V1',
    'middleware' => 'api',
    'prefix' => 'password'
], function () {
    Route::post('create', [PasswordResetController::class, 'create']);
    Route::get('find/{token}', [PasswordResetController::class, 'find']);
    Route::post('reset', [PasswordResetController::class, 'reset']);
});
//APP
Route::group([
    'prefix' => 'v1',
    'namespace' => 'Api\V1',
], function () {
    //register
    Route::post('sign-up', [UserController::class, 'register']);
    //login
    Route::post('sign-in', [UserController::class, 'login']);
    //login social
    Route::post('login-social', [SocialsController::class, 'login']);

    Route::group(['middleware' => 'auth.jwt'], function () {
        //logout
        Route::post('logout', [UserController::class, 'logout']);
        //get_user_info
        Route::get('get-user-info', [UserController::class, 'getUserInfo']);
        //update_user_info
        Route::post('update-user-info', [UserController::class, 'updateUserInfo']);
        //get_save_story
        Route::get('get-save-story', [UserController::class, 'getSaveStory']);
        //post_save_story
        Route::post('post-save-story', [UserController::class, 'postSaveStory']);
        //get_comment
        Route::get('get-user-comment', [UserController::class, 'getUserComment']);
        //post_comment
        Route::post('post-user-comment', [UserController::class, 'postUserComment']);
        //get_coin
        Route::get('get-coin', [UserController::class, 'getCoin']);
        //post_coin
        Route::post('post-add-coin', [UserController::class, 'postAddCoin']);
        //vote_star
        Route::post('post-vote-star', [UserController::class, 'postVoteStar']);
        //et_category
        Route::get('get-category', [CategoryController::class, 'getCategory']);
        //Home
        //get_story
        Route::get('get-story', [MyHomeController::class, 'getStory']);
        //info_story
        Route::get('info-story', [MyHomeController::class, 'infoStory']);
        //get_topic
        Route::get('get-topic', [MyHomeController::class, 'getTopic']);
        //get_home_story
        Route::get('get-home-story', [MyHomeController::class, 'getHomeStory']);
        //get_list_chapter
        Route::get('get-list-chapter', [MyHomeController::class, 'getListChapter']);
        //content_chapter
        Route::get('get-content-chapter', [MyHomeController::class, 'getContentChapter']);
        //read-chapter
        Route::post('read-chapter',[UserController::class,'readChapter']);
        //unlock-all-chapter
        Route::post('unlock-all-chapter',[UserController::class,'unlockAllChapter']);
        //unlock-one-chapter
        Route::post('unlock-one-chapter',[UserController::class,'unlockOneChapter']);

    });
    Route::middleware('jwt.refresh')->get('/token/refresh', [UserController::class, 'refresh']);

});

//CMS
Route::group([
    'prefix' => 'admin',
    'namespace' => 'admin',
], function () {
    //login_CMS
    Route::post('/login', [AdminController::class, 'loginAdmin']);
    //home_CMS
    Route::get('get-home-CMS', [CMSController::class, 'getHomeCMS']);
    //create_CMS
    Route::post('createCMS', [EditStoryController::class, 'createCMS']);
    //edit_CMS
    Route::get('editCMS', [EditStoryController::class, 'editCMS']);
    //update_CMS
    Route::post('updateCMS', [EditStoryController::class, 'updateCMS']);
    //delete_CMS
    Route::post('deleteCMS', [EditStoryController::class, 'deleteCMS']);
    //searchCMS
    Route::get('searchCMS',[EditStoryController::class,'searchCMS']);
});

// Google Sign In
Route::post('/get-google-sign-in-url', [SocialsController::class, 'getGoogleSignInUrl']);
Route::get('/callback', [SocialsController::class, 'loginCallback']);

// Facebook Login URL
Route::prefix('facebook')->group( function(){
    Route::get('auth', [SocialsController::class, 'loginUsingFacebook']);
    Route::get('callback', [SocialsController::class, 'callbackFromFacebook']);
});

