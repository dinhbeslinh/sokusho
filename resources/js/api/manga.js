import axios from 'axios';

export const  getMangaApi = async() => {
    const res = await axios.get('https://dokusho.examtoeic.online/api/admin/get-home-CMS')
    return res.data.data
    // console.log(res.data.data);
}

// export const createMangaApi = async(manga) => {
//
//     const res = await axios.post('http://127.0.0.1:8000/api/admin/createnewcms',{
//     headers: {
//         'Content-Type': 'application/json'
//     },
//         data: manga
//     })
//         .then((res) => {
//             (JSON.stringify(res.data));
//         })
//     console.log(res.data)
// }
export const createMangaApi = async(manga) => {
    const res = await axios.post('https://dokusho.examtoeic.online/api/admin/createCMS',manga)
    res.data.data
}




export const editMangaApi = async(id) => {
    const res = await  axios.get(`https://dokusho.examtoeic.online/api/admin/editCMS?story_id=${id}`)
    return res.data
}
