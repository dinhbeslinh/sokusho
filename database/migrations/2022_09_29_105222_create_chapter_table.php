<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChapterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chapter', function (Blueprint $table) {
            $table->id();
            $table->integer('number');
            $table->string('name_chapter')->nullable();
            $table->longText('content_chapter');
            $table->bigInteger('story_id')->unsigned()->index()->nullable();
            $table->foreign('story_id')->references('id')->on('story')->onDelete('cascade');
            $table->string('slug_chapter')->nullable();
            $table->integer('user_id')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chapter');
    }
}
