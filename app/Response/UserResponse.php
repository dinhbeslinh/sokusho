<?php


namespace App\Response;


class UserResponse
{
    public $id;
    public $name;
    public $avatar;
    public $coin;
    public $nick_name;
    public $date_of_birth;
}
