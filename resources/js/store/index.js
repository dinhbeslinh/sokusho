import { createStore } from "vuex";
import login from "./modules/login"
import manga from "./modules/manga"


export default createStore({
    state: {},
    getters: {},
    mutations: {},
    actions: {},
    modules: {
        login,
        manga,
    },
});
